import os
from flask import Flask, request
import sqlite3

app = Flask(__name__)

# Check if the database file exists, if not, create it
if not os.path.exists('app.db'):
    # Create the database connection
    conn = sqlite3.connect('app.db')

    # Create a cursor object to execute SQL commands
    cursor = conn.cursor()

    # Create the files table if it doesn't exist
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS files (
            fileID INTEGER PRIMARY KEY,
            fileName TEXT,
            fileContent TEXT
        )
    ''')

    # Commit the changes to the database
    conn.commit()

    # Close the database connection
    conn.close()

# Create the "archive" folder if it doesn't exist
archive_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'archive')
if not os.path.exists(archive_folder):
    os.makedirs(archive_folder)

@app.route("/", methods=["POST"])
def process_file():
    is_db = request.form.get("isDB", "false").lower()
    file = request.files.get("file")

    if is_db == "true":
        # Read the file content
        file_content = file.read().decode()

        # Save the file content to the database
        conn = sqlite3.connect('app.db')
        cursor = conn.cursor()
        cursor.execute("INSERT INTO files (fileName, fileContent) VALUES (?, ?)", (file.filename, file_content))
        conn.commit()
        conn.close()

        # Save the file in the archive folder
        file_path = archive_folder + '/' + file.filename
        with open(file_path, "w") as f:
            f.write(file_content)

        return file.filename + '\n', 200
    elif is_db == "false":
        # Return the content of the file
        return file.read().decode() + '\n', 200
    else:
        # Return an error message for invalid isDB value
        return "Invalid value for isDB parameter. Please provide 'true' or 'false'.\n", 400

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8080)
