The assignment:
reate a System:
1. The system accepts CURL request with one parameter - isDB
2. if True: read a file, write it to DB, move the file to archive, and return the file name in the response
3. if False: read the file, and return the file content in response.

 Here's an explanation of the project structure and components I've been working on:

    main.py: This is the main Python script that defines the Flask application. It handles incoming requests, processes files, and interacts with the SQLite database. The script contains an endpoint ("/") that accepts a POST request with the isDB parameter and a file. Depending on the value of isDB, the file is either saved in the database or returned as the response.

    Dockerfile: The Dockerfile is used to build the Docker image for your Flask application. It specifies the base image as python:3.9, installs the necessary dependencies listed in requirements.txt, and copies the project files into the container. It also sets the working directory and exposes the port 8080 for communication.

    docker-compose.yml: The docker-compose.yml file defines the services and their configurations for running your application. In this case, it specifies a service named app based on the Dockerfile, maps the container's port 8080 to the host's port 8080, and mounts the archive directory and the DB as volumes to persist the importent uploaded files.

    requirements.txt: This file lists the Python packages and their versions required for your application. It ensures that the correct versions of Flask and other dependencies are installed when building the Docker image.

    archive directory: This directory is created by the application if it doesn't exist. It serves as the storage location for the uploaded files. When a file is saved, it is stored in this directory both on the host machine (if using Docker volumes) and inside the container.


How to use the system?

1. To run your application, you need to have Docker and Docker Compose installed on your system. 

2. Execute docker-compose up from the project directory. This will build the Docker image (if not already built) and start the container :
docker-compose up --build

3. Run from your terminal the command:
curl -X POST -F "isDB=*here you put true/false/something else*" -F "file=@*here you put the path to any file you want from your computer*" http://localhost:8080/

4. If you want to see your DB you enter to the address https://sqliteviewer.app/

GOOD LUCK!


 
